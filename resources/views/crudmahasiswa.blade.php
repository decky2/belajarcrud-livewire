<!DOCTYPE html>
<html lang="en">

<head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <title>Livewire</title>
      <link rel="stylesheet" href="{{ asset('css/app.css') }}">
      <script src="https://cdn.tailwindcss.com"></script>
      <link rel="stylesheet" href="https://unpkg.com/flowbite@1.4.4/dist/flowbite.min.css" />
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
      <script src="https://unpkg.com/flowbite@1.4.0/dist/flowbite.js"></script>
      <script src="https://cdn.jsdelivr.net/npm/alpinejs@2.8.2"></script>
      @livewireStyles
</head>

<body>
      <nav class="bg-black w-full">
            <div class="w-full container mx-auto flex justify-center items-center py-3">
                  <a class="font-bold text-3xl text-white" href="/">Belajar Livewire</a>
            </div>
      </nav>
      <div class="">
            <div class="container mx-auto">
                  @livewire('mahasiswa')
            </div>
            
      </div>
@livewireScripts
</body>

</html>
