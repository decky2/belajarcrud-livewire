<div>
    <div class="my-3 p-3 bg-body rounded shadow-sm">
        <!-- Formulir dengan Encoding Type untuk Mengunggah Berkas -->
        <form enctype="multipart/form-data" wire:submit.prevent="store">
            <!-- Field Nama -->
            <div class="w-full flex flex-row gap-5 my-3 items-center">
                <label for="nama" class="block text-sm font-medium text-gray-900 w-[100px]">Nama</label>
                <div class="w-full flex flex-col gap-1">
                    <input type="text" class="block w-full text-sm text-gray-900 border border-gray-300 rounded-lg cursor-pointer " wire:model="nama_lengkap">
                    @error('nama_lengkap') <span class="text-danger">{{ $message }}</span>@enderror
                </div>
            </div>
            <!-- Field NIM -->
            <div class="w-full flex flex-row gap-5 my-3 items-center">
                <label for="nim" class="block text-sm font-medium text-gray-900 w-[100px]">NIM</label>
                <div class="w-full flex flex-col gap-1">
                    <input type="text" class="block w-full text-sm text-gray-900 border border-gray-300 rounded-lg cursor-pointer " wire:model="nim">
                        @error('nim')
                        @if ($message === 'The nim has already been taken.')
                            <span class="text-danger">NIM sudah ada.</span>
                        @else
                            <span class="text-danger">{{ $message }}</span>
                        @endif
                    @enderror 
                </div>
            </div>
            <!-- Field Jenis Kelamin-->
            <div class="w-full flex flex-row gap-5 my-3 items-center">
                <label for="jenis_kelamin" class="block text-sm font-medium text-gray-900 w-[100px]">Jenis Kelamin</label>
                <div class="w-full flex flex-col gap-1">
                    <select id="jenis_kelamin" class="block w-full text-sm text-gray-900 border border-gray-300 rounded-lg cursor-pointer p-2.5" wire:model="jenis_kelamin">
                        <option selected value="">Pilih Jenis Kelamin</option>
                        <option value="L">Laki-laki</option>
                        <option value="P">Perempuan</option>
                    </select>
                    @error('jenis_kelamin') <span class="text-danger">{{ $message }}</span>@enderror

                </div>
            </div>
            <!-- Field Tempat Lahir -->
            <div class="w-full flex flex-row gap-5 my-3 items-center">
                <label for="tempat_lahir" class="block text-sm font-medium text-gray-900 w-[100px]">Tempat Lahir</label>
                <div class="w-full flex flex-col gap-1">
                    <input type="text" class="block w-full text-sm text-gray-900 border border-gray-300 rounded-lg cursor-pointer " wire:model="tempat_lahir">
                        @error('tempat_lahir') <span class="text-danger">{{ $message }}</span>@enderror

                </div>
            </div>
            <!-- Field Tanggal Lahir -->
            <div class="w-full flex flex-row gap-5 my-3 items-center">
                <label for="tanggal_lahir" class="block text-sm font-medium text-gray-900 w-[100px]">Tanggal Lahir</label>
                <div class="w-full flex flex-col gap-1">
                    <input type="date" class="block w-full text-sm text-gray-900 border border-gray-300 rounded-lg cursor-pointer " wire:model="tanggal_lahir">
                        @error('tanggal_lahir') <span class="text-danger">{{ $message }}</span>@enderror

                </div>
            </div>
            <!-- Field Email -->
            <div class="w-full flex flex-row gap-5 my-3 items-center">
                <label for="email" class="block text-sm font-medium text-gray-900 w-[100px]">Email</label>
                <div class="w-full flex flex-col gap-1">
                    <input type="email" class="block w-full text-sm text-gray-900 border border-gray-300 rounded-lg cursor-pointer " wire:model="email">
                        @error('email') <span class="text-danger">{{ $message }}</span>@enderror

                </div>
            </div>
            <!-- Field Nomor Telepon -->
            <div class="w-full flex flex-row gap-5 my-3 items-center">
                <label for="no_telepon" class="block text-sm font-medium text-gray-900 w-[100px]">Nomor Telepon</label>
                <div class="w-full flex flex-col gap-1">
                    <input type="text" class="block w-full text-sm text-gray-900 border border-gray-300 rounded-lg cursor-pointer " wire:model="nomor_telepon">
                        @error('nomor_telepon') <span class="text-danger">{{ $message }}</span>@enderror

                </div>
            </div>
            <!-- Field Alamat -->
            <div class="w-full flex flex-row gap-5 my-3 items-center">
                <label for="alamat" class="block text-sm font-medium text-gray-900 w-[100px]">Alamat</label>
                <div class="w-full flex flex-col gap-1">
                    <textarea type="text" class="block w-full text-sm text-gray-900 border border-gray-300 rounded-lg cursor-pointer h-[100px]" wire:model="alamat_lengkap"></textarea>
                        @error('alamat_lengkap') <span class="text-danger">{{ $message }}</span>@enderror

                </div>
            </div>
            <!-- Field Select Picture -->
            <div class="w-full flex flex-row gap-5 my-3 items-center">
                <label class="block text-sm font-medium text-gray-900 w-[100px]" for="file_input">Upload file</label>
                <div class="w-full flex flex-col gap-1">
                    <input class="block w-full text-sm text-gray-900 border border-gray-300 rounded-lg cursor-pointer " id="file_input" type="file" enctype="multipart/form-data" wire:model="foto_profil">
                    @error('foto_profil') <span class="text-danger">{{ $message }}</span>@enderror

                </div>
            </div>
            <!-- Tombol Simpan -->
            <div class="w-full flex flex-col gap-3">
                <div class="w-full">
                    <button type="button" class="w-full py-3 bg-blue-600 text-white rounded-xl" name="submit" wire:click.prevent="store">SIMPAN</button>
                </div>
                <div class="w-full">
                    <button type="button" class="w-full py-3 bg-blue-600 text-white rounded-xl" @click="showModalAdd= false">CANCEL</button>
                </div>
            </div>
        </form>
    </div>
    
</div>