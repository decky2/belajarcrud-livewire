<?php

namespace App\Livewire;

use Livewire\Component;
use \App\Models\Mahasiswa as TabelMahasiswa;
use Livewire\WithFileUploads;



class Mahasiswa extends Component
{
    // Menggunakan fitur Livewire WithFileUploads
    use WithFileUploads;
    // Properti untuk menyimpan data mahasiswa
    public $mahasiswaId, $nama_lengkap, $nim, $jenis_kelamin, $tempat_lahir, $tanggal_lahir, $email, $nomor_telepon, $alamat_lengkap, $updateMahasiswa= false;
    // Properti untuk menyimpan foto profil
    public $foto_profil, $oldfoto_profil, $updateFotoProfil = false;

    public $filterJenisKelamin;
    public $search = '';

    // Fungsi untuk merender tampilan
    public function render()
    {
        $query = TabelMahasiswa::query();

        if ($this->search) {
            $query->where(function ($q) {
                $q->where('nama_lengkap', 'like', '%' . $this->search . '%')
                    ->orWhere('nim', 'like', '%' . $this->search . '%')
                    ->orWhere('tempat_lahir', 'like', '%' . $this->search . '%');
            });
        }
    
        if ($this->filterJenisKelamin) {
            $query->where('jenis_kelamin', $this->filterJenisKelamin);
        }
    
        $mahasiswas = $query->paginate(5);
    
        return view('livewire.mahasiswa', compact('mahasiswas'));
    }

    public function searchMahasiswa()
    {
        $this->dispatch('filterJenisKelamin');
    }

     // Aturan validasi untuk formulir
    public function rules()
    {
        return [
            'nama_lengkap' => 'required',
            'nim' => $this->updateMahasiswa ? 'required' : 'required|unique:mahasiswas,nim',
            'jenis_kelamin' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required|date',
            'email' => 'required|email',
            'nomor_telepon' => 'required|numeric',
            'alamat_lengkap' => 'required',
            'foto_profil' => 'required|image|max:2048',
        ];
        
    }

        public function resetFilters()
    {
        $this->filterJenisKelamin = '';
        $this->filterNama = '';
        $this->filterNIM = '';
        $this->filterTempatLahir = '';
    }

    // Fungsi untuk mereset nilai properti
    public function resetFields()
    {
        $this->nama_lengkap = '';
        $this->nim = '';
        $this->jenis_kelamin = '';
        $this->tempat_lahir = '';
        $this->tanggal_lahir = '';
        $this->email = '';
        $this->nomor_telepon = '';
        $this->alamat_lengkap = '';
        $this->foto_profil = '';
    }

    

     // Fungsi untuk menampilkan formulir penambahan data
    public function create()
    {
        $this->resetFields();
        $this->updateMahasiswa = false;
    }

     // Fungsi untuk menyimpan data mahasiswa ke database
    public function store()
    { 
        // Mengecek apakah dalam mode pembaruan
        if ($this->updateMahasiswa) {
            unset($this->rules['nim']['unique']);
        }
        if ($this->foto_profil == null){ 
            $filename = ""; 
        }else{ 
            $filename = $this->foto_profil->store('photos', 'public'); 
        }
         // Mengecek apakah ada foto profil yang diunggah
        $this->validate(); 
        try{
            // Menyimpan data mahasiswa ke dalam database
            TabelMahasiswa::create([
                'id'=>uuid_create(),
                'nama_lengkap' => $this->nama_lengkap,
                'nim' => $this->nim,
                'jenis_kelamin' => $this->jenis_kelamin,
                'tempat_lahir' => $this->tempat_lahir,
                'tanggal_lahir' => $this->tanggal_lahir,
                'email' => $this->email,
                'nomor_telepon' => $this->nomor_telepon,
                'alamat_lengkap' => $this->alamat_lengkap,
                'foto_profil' => $filename,
            ]);
            // Menampilkan pesan sukses
            session()->flash('success', 'Data Mahasiswa Created Successfully!!');
            // Mereset nilai properti dan menutup formuli
            $this->resetFields();
            }catch (\Exception $ex) {
                // Menampilkan pesan kesalahan
                session()->flash('error', 'Something goes wrong!!');
        }
    }



     // Fungsi untuk menampilkan formulir pembaruan data
    public function edit($id)
    {
        try {
             // Mencari data mahasiswa berdasarkan ID
            $mahasiswa = TabelMahasiswa::findOrFail($id);
            // Menampilkan pesan kesalahan jika data tidak ditemukan
            if (!$mahasiswa) {
                session()->flash('error', 'Data Mahasiswa not found');
            } else {
                // Mengisi properti dengan nilai data mahasiswa yang akan diperbarui
                $this->nama_lengkap = $mahasiswa->nama_lengkap;
                $this->nim = $mahasiswa->nim;
                $this->jenis_kelamin = $mahasiswa->jenis_kelamin;
                $this->tempat_lahir = $mahasiswa->tempat_lahir;
                $this->tanggal_lahir = $mahasiswa->tanggal_lahir;
                $this->email = $mahasiswa->email;
                $this->nomor_telepon = $mahasiswa->nomor_telepon;
                $this->alamat_lengkap = $mahasiswa->alamat_lengkap;
                $this->foto_profil = $mahasiswa->foto_profil;
                $this->oldfoto_profil = $mahasiswa->foto_profil;
                $this->mahasiswaId = $mahasiswa->id;
                $this->updateMahasiswa = true;
            }
        } catch (\Exception $ex) {
            // Menampilkan pesan kesalahan
            session()->flash('error', 'Something goes wrong!!');
        }
    }




    // Fungsi untuk menyimpan pembaruan data mahasiswa ke database
    public function update()
    {
        $this->validate([
            'nama_lengkap' => 'required',
            'nim' => 'required',
            'jenis_kelamin' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required|date',
            'email' => 'required|email',
            'nomor_telepon' => 'required|numeric',
            'alamat_lengkap' => 'required',
            'foto_profil' => 'required|image|max:2048',
        ]);

        // Mencari data mahasiswa yang akan diperbarui
        $mahasiswa = TabelMahasiswa::find($this->mahasiswaId);

        // Menghapus foto profil lama
        if (file_exists('storage/' . $mahasiswa->foto_profil)) {
            unlink('storage/' . $mahasiswa->foto_profil);
        }
        // Menyimpan foto profil baru
        $filename = $this->foto_profil->store('photos', 'public');

        try {
            // Memperbarui data mahasiswa
            $mahasiswa->update([
                'nama_lengkap' => $this->nama_lengkap,
                'nim' => $this->nim,
                'jenis_kelamin' => $this->jenis_kelamin,
                'tempat_lahir' => $this->tempat_lahir,
                'tanggal_lahir' => $this->tanggal_lahir,
                'email' => $this->email,
                'nomor_telepon' => $this->nomor_telepon,
                'alamat_lengkap' => $this->alamat_lengkap,
                'foto_profil' => $filename,
            ]);

             // Menampilkan pesan sukses dan mereset nilai properti
            session()->flash('success', 'Data Mahasiswa Successfully!!');
            $this->resetFields();
            $this->updateMahasiswa = false;
        } catch (\Exception $ex) {
            // Menampilkan pesan kesalahan
            session()->flash('error', 'Something goes wrong!!');
        }
    }

    // Fungsi untuk menghapus data mahasiswa dari database
    public function delete($id)
    {
        try{
            // Mencari data mahasiswa yang akan dihapus dan Menghapus data mahasiswa dan foto profil dari penyimpanan
            $mahasiswa = TabelMahasiswa::find($id);
            unlink('storage/'.$mahasiswa->foto_profil);
            $mahasiswa ->delete();
            // Mereset nilai properti dan menampilkan pesan sukses
            $this->resetFields();
            session()->flash('success',"Post Deleted Successfully!!");
        }catch(\Exception $e){
            // Menampilkan pesan kesalahan
            session()->flash('error',"Something goes wrong!!");
        }
    }
}